export interface Technology {
	id?: string | number;
	name: string;
	img: string;
	price: string;

	anchor?: string;
	isNew?: boolean;
}
