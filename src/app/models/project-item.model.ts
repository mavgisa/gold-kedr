export interface ProjectItemModel {
	id: string;
	heading: string;
	descriptionList: string[];
	costText: string;
	cost: string;
	hasConsultingButton: boolean;
	images: Array<{
		small: string;
		medium: string;
		big: string;
	}>;
}
