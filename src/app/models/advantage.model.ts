export interface Advantage {
	images: string[];
	advantages: string[];
	anchor: string;
	title: string;
}
