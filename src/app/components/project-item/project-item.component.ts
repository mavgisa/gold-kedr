import { Component, Input, OnInit } from '@angular/core';
import { NgxGalleryAnimation, NgxGalleryOptions } from 'ngx-gallery';
import { RoutingConst } from '../../const/routing.const';
import { ProjectItemModel } from '../../models/project-item.model';
import { ApplicationService } from '../../services/application.service';

@Component({
	selector: 'app-project-item',
	templateUrl: './project-item.component.html',
})
export class ProjectItemComponent implements OnInit {
	readonly galleryOptions: NgxGalleryOptions[] = [
		{
			width: '100%',
			height: '400px',
			thumbnailsColumns: 4,
			imageAnimation: NgxGalleryAnimation.Slide,
		},
		// max-width 800
		{
			breakpoint: 800,
			width: '100%',
			height: '300px',
			imagePercent: 80,
			thumbnailsPercent: 20,
			thumbnailsMargin: 10,
			thumbnailMargin: 10,
			imageArrows: false,
			imageSwipe: true,
		},
		// max-width 400
		{
			breakpoint: 400,
			preview: false,
		},
	];

	@Input() item: ProjectItemModel;

	constructor(private appService: ApplicationService) {}

	ngOnInit() {
		if (!this.item) {
			throw new Error(`No item!`);
		}
	}

	goBack() {
		this.appService.goToPage(RoutingConst.PROJECTS.name);
	}

	openConsultingForm() {
		alert(`Скоро будет этот функционал`);
	}
}
