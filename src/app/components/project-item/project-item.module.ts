import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { NgxGalleryModule } from 'ngx-gallery';
import { ProjectItemComponent } from './project-item.component';

@NgModule({
	declarations: [ProjectItemComponent],
	imports: [CommonModule, NgxGalleryModule, TranslateModule],
	exports: [ProjectItemComponent],
})
export class ProjectItemModule {}
