import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AdvantagePointComponent } from './advantage-point.component';

@NgModule({
	declarations: [AdvantagePointComponent],
	exports: [AdvantagePointComponent],
	imports: [CommonModule],
})
export class AdvantagePointModule {}
