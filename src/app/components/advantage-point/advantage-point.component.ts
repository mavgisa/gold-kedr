import { Component, Input, OnInit } from '@angular/core';

@Component({
	selector: 'app-advantage-point',
	templateUrl: './advantage-point.component.html',
})
export class AdvantagePointComponent implements OnInit {
	@Input() advantage: {
		title: string;
		description: string;
	};

	constructor() {}

	ngOnInit() {}
}
