import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectItemModule } from '../../project-item/project-item.module';
import { ProjectsHousesComponent } from './projects-houses.component';

@NgModule({
	declarations: [ProjectsHousesComponent],
	imports: [CommonModule, ProjectItemModule, TranslateModule],
	exports: [ProjectsHousesComponent],
})
export class ProjectsHousesModule {}
