import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectItemModule } from '../../project-item/project-item.module';
import { ProjectsAlcovesComponent } from './projects-alcoves.component';

@NgModule({
	declarations: [ProjectsAlcovesComponent],
	imports: [CommonModule, TranslateModule, ProjectItemModule],
	exports: [ProjectsAlcovesComponent],
})
export class ProjectsAlcovesModule {}
