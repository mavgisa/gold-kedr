import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectItemModule } from '../../project-item/project-item.module';
import { ProjectsBathesComponent } from './projects-bathes.component';

@NgModule({
	declarations: [ProjectsBathesComponent],
	imports: [CommonModule, ProjectItemModule, TranslateModule],
	exports: [ProjectsBathesComponent],
})
export class ProjectsBathesModule {}
