import { Component, OnInit } from '@angular/core';
import { RoutingConst } from '../../../const/routing.const';
import { ProjectItemModel } from '../../../models/project-item.model';
import { ApplicationService } from '../../../services/application.service';

@Component({
	selector: 'app-projects-bathes',
	templateUrl: './projects-bathes.component.html',
})
export class ProjectsBathesComponent implements OnInit {
	readonly bathes: ProjectItemModel[] = [
		{
			id: '1',
			heading: 'Проект дома "Венская сладность"',
			descriptionList: ['- Площадь дома: 100 м2', '- 3 отдельные комнаты (спальни)', '- кухня-гостиная', '- котельная', '- 2 санузла', '- кладовая'],
			costText: 'Стоимость домокомплекта:',
			cost: '39.000 $',
			hasConsultingButton: true,
			images: [
				{
					small: 'https://static.tildacdn.com/tild6230-6630-4437-b137-626631633930/_31.jpg',
					medium: 'https://static.tildacdn.com/tild6230-6630-4437-b137-626631633930/_31.jpg',
					big: 'https://static.tildacdn.com/tild6230-6630-4437-b137-626631633930/_31.jpg',
				},
				{
					small: 'https://static.tildacdn.com/tild6430-6432-4566-a638-363833613532/_32.jpg',
					medium: 'https://static.tildacdn.com/tild6430-6432-4566-a638-363833613532/_32.jpg',
					big: 'https://static.tildacdn.com/tild6430-6432-4566-a638-363833613532/_32.jpg',
				},
			],
		},
	];

	constructor(private appService: ApplicationService) {}

	ngOnInit() {}
}
