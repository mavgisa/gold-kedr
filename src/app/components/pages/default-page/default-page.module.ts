import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { AboutModule } from '../../../about/about.module';
import { AdvantagePointModule } from '../../advantage-point/advantage-point.module';
import { AdvantagesModule } from '../../advantages/advantages.module';
import { HeaderModule } from '../../header/header.module';
import { TechnologiesModule } from '../../technologies/technologies.module';
import { DefaultPageComponent } from './default-page.component';

@NgModule({
	declarations: [DefaultPageComponent],
	imports: [CommonModule, TranslateModule, TechnologiesModule, AdvantagesModule, AdvantagePointModule, HeaderModule, AboutModule],
})
export class DefaultPageModule {}
