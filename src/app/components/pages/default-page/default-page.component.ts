import { Component, OnInit } from '@angular/core';
import { RoutingConst } from '../../../const/routing.const';
import { ApplicationService } from '../../../services/application.service';

@Component({
	selector: 'app-default-page',
	templateUrl: './default-page.component.html',
})
export class DefaultPageComponent implements OnInit {
	subscription;
	readonly OUR_ADVANTAGES = [
		{
			title: 'Надёжность',
			description: 'В строительстве используются многолетние хвойные породы древесины: северная сосна, сибирский кедр, ель, лиственница, пихта.',
		},
		{
			title: 'Золотые сроки золотого кедра',
			description: 'Компания осуществит строительство дома "под ключ" в течение одного, двух месяцев в зависимости от сложности проекта.',
		},
		{
			title: 'Долговечность',
			description: 'Построенный вместе с нами дом будет радовать Вас и Ваших правнуков.',
		},
		{
			title: 'Золотые партнёры золотого кедра',
			description: 'Агентство недвижимости "Мариэлт". Подробнее: Наши партнёры',
		},
		{
			title: 'Золотые архитекторы и конструкторы золотого кедра',
			description:
				'Работники компании разработают для вас индивидуальный проект дома любой сложности, создадут 3D модель и подготовят всю необходимую документацию.',
		},
	];

	constructor(private appService: ApplicationService) {}

	ngOnInit() {}

	goToProjectsPage() {
		this.appService.goToPage(RoutingConst.PROJECTS.link);
	}
}
