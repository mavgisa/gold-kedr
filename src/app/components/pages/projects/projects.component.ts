import { Component, OnInit } from '@angular/core';
import { RoutingConst } from '../../../const/routing.const';
import { ApplicationService } from '../../../services/application.service';

@Component({
	selector: 'app-projects',
	templateUrl: './projects.component.html',
})
export class ProjectsComponent implements OnInit {
	readonly projects = [
		{
			title: 'Проекты домов',
			img: '/assets/img/house_1.jpg',
			link: RoutingConst.HOUSES_PROJECT.name,
		},
		{
			title: 'Проекты бань',
			img: '/assets/img/house_1.jpg',
			link: RoutingConst.BATHES_PROJECT.name,
		},
		{
			title: 'Проекты беседок',
			img: '/assets/img/house_1.jpg',
			link: RoutingConst.ALCOVE_PROJECT.name,
		},
	];

	constructor(private appService: ApplicationService) {}

	ngOnInit() {}

	goToProjectPage({ img, title, link }) {
		this.appService.goToPage(link);
	}

	goBack() {
		this.appService.goToPage(RoutingConst.DEFAULT_ROUTE.link);
	}
}
