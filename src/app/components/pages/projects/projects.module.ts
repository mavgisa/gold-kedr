import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectsComponent } from './projects.component';

@NgModule({
	declarations: [ProjectsComponent],
	imports: [CommonModule, TranslateModule],
})
export class ProjectsModule {}
