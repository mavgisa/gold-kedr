import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as bodyScrollLock from 'body-scroll-lock';
import { ImageConst } from '../../const/image.const';
import { RoutingConst } from '../../const/routing.const';
import { ApplicationService } from '../../services/application.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
	readonly HEADER_LOGO = ImageConst.HEADER_LOGO;

	readonly HEADER_ELEMENTS = [
		{
			name: 'MENU.COMPANY',
			link: RoutingConst.DEFAULT_ROUTE.name,
			anchor: 'about',
		},
		{
			name: 'MENU.TECHNOLOGIES',
			link: RoutingConst.DEFAULT_ROUTE.name,
			anchor: 'technologies',
		},
		{
			name: 'MENU.PARTNERS',
			link: RoutingConst.DEFAULT_ROUTE.name,
			anchor: 'partners',
		},
		{
			name: 'MENU.PROJECTS',
			link: RoutingConst.PROJECTS.name,
		},
		{
			name: 'MENU.CONTACTS',
			link: RoutingConst.DEFAULT_ROUTE.name,
			anchor: 'contacts',
		},
	];

	isMenuOpen: boolean = false;

	@ViewChild('menu') menu: ElementRef;

	constructor(private applicationService: ApplicationService) {}

	ngOnInit() {}

	toggleMenu() {
		this.isMenuOpen = !this.isMenuOpen;
		this.applicationService.takeLeftMenuStatus(this.isMenuOpen);
		this.isMenuOpen ? this.disableBodyScroll() : this.enableBodyScroll();
	}

	goToPageOrAnchor({ anchor, link }) {
		this.closeMenu();

		this.applicationService.goToPage(link);

		if (anchor) {
			this.applicationService.goToAnchor(anchor);
		}
	}

	closeMenu() {
		this.isMenuOpen = false;

		this.enableBodyScroll();
	}

	disableBodyScroll() {
		bodyScrollLock.disableBodyScroll(this.menu.nativeElement);
	}

	enableBodyScroll() {
		bodyScrollLock.enableBodyScroll(this.menu.nativeElement);
	}
}
