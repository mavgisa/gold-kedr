import { Component, OnInit } from '@angular/core';
import { Technology } from '../../models/technology.model';

@Component({
	selector: 'app-technologies',
	templateUrl: './technologies.component.html',
})
export class TechnologiesComponent implements OnInit {
	readonly TECHNOLOGIES: Technology[] = [
		{
			name: 'Деревянный монолитный дом',
			price: '390 $/м2',
			img: '/assets/img/house_3.jpg',
			anchor: 'monolithic',
			isNew: true,
		},
		{
			name: 'Каркасный дом',
			price: '320 $/м2',
			img: '/assets/img/house_2.jpg',
			anchor: 'skeleton',
			isNew: false,
		},
		{
			name: 'Дом из двойного бруса',
			price: '375 $/м2',
			img: '/assets/img/house_1.jpg',
			anchor: 'double',
			isNew: false,
		},
	];

	constructor() {}

	ngOnInit() {}
}
