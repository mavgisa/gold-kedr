import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { TechnologiesComponent } from './technologies.component';
import { TechnologyModule } from './technology/technology.module';

@NgModule({
	declarations: [TechnologiesComponent],
	imports: [CommonModule, TranslateModule, TechnologyModule],
	exports: [TechnologiesComponent],
})
export class TechnologiesModule {}
