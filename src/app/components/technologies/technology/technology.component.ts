import { Component, Input, OnInit } from '@angular/core';
import { Technology } from '../../../models/technology.model';
import { ApplicationService } from '../../../services/application.service';

@Component({
	selector: 'app-technology',
	templateUrl: './technology.component.html',
})
export class TechnologyComponent implements OnInit {
	@Input() technology: Technology;

	isShowMoreBtn: boolean = false;

	constructor(private appService: ApplicationService) {}

	ngOnInit() {}

	mouseEnter() {
		this.isShowMoreBtn = true;
	}

	mouseLeave() {
		this.isShowMoreBtn = false;
	}

	goToTechnology({ anchor }: Technology) {
		this.appService.goToAnchor(anchor);
	}
}
