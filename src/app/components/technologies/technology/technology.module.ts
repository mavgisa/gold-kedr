import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { TechnologyComponent } from './technology.component';

@NgModule({
	declarations: [TechnologyComponent],
	exports: [TechnologyComponent],
	imports: [CommonModule, TranslateModule],
})
export class TechnologyModule {}
