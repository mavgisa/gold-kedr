import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AdvantageModule } from './advantage/advantage.module';
import { AdvantagesComponent } from './advantages.component';

@NgModule({
	declarations: [AdvantagesComponent],
	imports: [CommonModule, AdvantageModule],
	exports: [AdvantagesComponent],
})
export class AdvantagesModule {}
