import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AdvantageComponent } from './advantage.component';

@NgModule({
	declarations: [AdvantageComponent],
	exports: [AdvantageComponent],
	imports: [CommonModule],
})
export class AdvantageModule {}
