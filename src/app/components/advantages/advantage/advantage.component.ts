import { Component, Input, OnInit } from '@angular/core';
import { Advantage } from '../../../models/advantage.model';

@Component({
	selector: 'app-advantage',
	templateUrl: './advantage.component.html',
})
export class AdvantageComponent implements OnInit {
	@Input() advantage: Advantage;

	constructor() {}

	ngOnInit() {}
}
