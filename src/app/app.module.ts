import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxGalleryModule } from 'ngx-gallery';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AboutModule } from './about/about.module';
import { AppComponent } from './app.component';
import { AppRouting } from './appRouting';
import { HeaderModule } from './components/header/header.module';
import { DefaultPageModule } from './components/pages/default-page/default-page.module';
import { ProjectsModule } from './components/pages/projects/projects.module';
import { ApplicationService } from './services/application.service';
import { HttpInterceptorService } from './services/http-interceptor.service';

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		NgxSpinnerModule,
		HttpClientModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [HttpClient],
			},
		}),
		AppRouting,

		// app components
		DefaultPageModule,
		ProjectsModule,
		AboutModule,
		NgxGalleryModule,
		HeaderModule,
	],
	providers: [
		ApplicationService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: HttpInterceptorService,
			multi: true,
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
