import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';

@Injectable()
export class ApplicationService {
	private leftMenuStatus: boolean = false;

	constructor(public spinner: NgxSpinnerService, public router: Router) {}

	goToPage(link: string) {
		this.router.navigateByUrl(link);
	}

	goToAnchor(link: string) {
		const el = document.querySelector(`#${link}`);

		if (el && el.scrollIntoView) {
			el.scrollIntoView();
		}
	}

	takeLeftMenuStatus(statusLeftMenu: boolean) {
		this.leftMenuStatus = statusLeftMenu;
	}

	getLeftMenuStatus() {
		console.log('123', this.leftMenuStatus);
		// return this.leftMenuStatus;
	}
}
