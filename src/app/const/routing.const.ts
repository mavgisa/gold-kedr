export class RoutingConst {
	static readonly DEFAULT_ROUTE = {
		name: '',
		link: '',
	};

	static readonly PROJECTS = {
		name: 'projects',
		link: '/projects',
	};

	static readonly HOUSES_PROJECT = {
		name: 'project-houses',
		link: '/project-houses',
	};

	static readonly BATHES_PROJECT = {
		name: 'project-bathes',
		link: '/project-bathes',
	};

	static readonly ALCOVE_PROJECT = {
		name: 'project-alcoves',
		link: '/project-alcoves',
	};
}
