import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DefaultPageComponent } from './components/pages/default-page/default-page.component';
import { ProjectsComponent } from './components/pages/projects/projects.component';
import { RoutingConst } from './const/routing.const';
import { ProjectsAlcovesComponent } from './components/pages/projects-alcoves/projects-alcoves.component';
import { ProjectsBathesComponent } from './components/pages/projects-bathes/projects-bathes.component';
import { ProjectsHousesComponent } from './components/pages/projects-houses/projects-houses.component';

export const routes: Routes = [
	{
		path: RoutingConst.DEFAULT_ROUTE.name,
		component: DefaultPageComponent,
	},
	{
		path: RoutingConst.PROJECTS.name,
		component: ProjectsComponent,
	},
	{
		path: RoutingConst.HOUSES_PROJECT.name,
		component: ProjectsHousesComponent,
	},
	{
		path: RoutingConst.ALCOVE_PROJECT.name,
		component: ProjectsAlcovesComponent,
	},
	{
		path: RoutingConst.BATHES_PROJECT.name,
		component: ProjectsBathesComponent,
	},
	{
		path: '**',
		redirectTo: RoutingConst.DEFAULT_ROUTE.name,
		pathMatch: 'full',
	},
];

export const AppRouting: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
